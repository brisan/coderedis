<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/predis/src/Autoloader.php';


class Redis extends CI_Controller {

	private $redis;

	function __construct()
	{
		parent::__construct();
		Predis\Autoloader::register();
		$this->redis = new Predis\Client();
		/*
		$this->redis = = new Predis\Client([
			'scheme' => 'tcp',
			'host'   => '10.0.0.1',
			'port'   => 6379,
		]);
		*/
	}


	/**
	 * [index description]
	 * @return [type] [description]
	 */
	public function index()
	{

		$value = $this->redis->ping();

		echo $value;
	}

	/**
	 * [set description]
	 *
	 */
	public function set()
	{
		$array = array(
			'k1' => "valor1",
			'k2' => "valor2",
			'k3' => "valor3",
		);
		$value = json_encode($array);
		$key = "test";
		//$value = "Hello";
		$this->redis->set($key, $value);
	}

	public function get()
	{
		$key = "test";
		echo $this->redis->get($key);
	}


}
